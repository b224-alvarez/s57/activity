let collection = [];

// Write the queue functions below.
// Print
function print() {
  return collection;
}

// Enqueue
function enqueue(name) {
  collection.push(name);
  return collection;
}

// Dequeue first elemet
function dequeue() {
  collection.shift();
  return collection;
}

// Get first element
function front() {
  return collection[0];
}

// Get queue size
function size() {
  return collection.length;
}

function isEmpty() {
  if (collection.length == 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
